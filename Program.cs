﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HackerCampClient;
using Newtonsoft.Json;

namespace HackerCampClient
{
    class Program
    {
        static void  Main(string[] args)
        {
            var name = "Kristian";
            var basePath= ".\\Data";

            try{
                var result = DoWork(basePath,name).Result;
                Console.WriteLine(result.Message);        
                Console.ReadLine();
            }
            catch(Exception e){
                Console.WriteLine($"Exception in DoWork({basePath},{name}) -> {e.Message}");
            }
        }

        public static async Task<CorrectAnswer> DoWork(string path, string name){
            var assignmentsPath = $"{path}\\Assignments";
            var completedPath = $"{path}\\Completed";
            var client = new FamilyAssignmentClient();
            //client.BaseUrl = "https://localhost:5001";
            AssignmentWrapper assignmentWrapper = null;
            if(OnGoingAssignmentExist(assignmentsPath)){
                Console.WriteLine("Assignments already present-> picking the latest");
                assignmentWrapper = await OpenAssignmentFromDisc(assignmentsPath);
            }
            else{
                Console.WriteLine("No assignments present-> getting a new one");
                assignmentWrapper = await client.GetAssignmentAsync(name: name, count: null);
                await SaveAssignmentToDiskAsync(assignmentWrapper.Id, assignmentWrapper.ToJson());
            }
            List<AnswerDTO> answers = CalculateFamilyTree(assignmentWrapper);
            var result = await client.PostAnswerAsync(assignmentWrapper.Id,answers);
            
             MoveFinishedAssignmentAway(completedPath,assignmentsPath,assignmentWrapper.Id);
            return result;
        }

        private static List<AnswerDTO> CalculateFamilyTree(AssignmentWrapper assignmentWrapper)
        {
            var result = new List<AnswerDTO>();
          
            //TODO: Implement a solution here

            return result;
        }

#region FileHandling
        private static void MoveFinishedAssignmentAway(string completedPath, string assignmentsPath,Guid id)
        {
            if(!Directory.Exists(completedPath))
            {
                Directory.CreateDirectory(completedPath);
            }
            File.Move($"{assignmentsPath}\\{id}",$"{completedPath}\\{id}");
        }

        private static bool OnGoingAssignmentExist(string path)
        {
            if(Directory.Exists(path)){
                var di = new DirectoryInfo(path);
                var files = di.EnumerateFiles();
                return files.Count()>0;
            }
            return false;
        }

        private static async Task<AssignmentWrapper> OpenAssignmentFromDisc(string path)
        {
            Guid id = Guid.Empty;
            if(Directory.Exists(path)){
                var di = new DirectoryInfo(path);
                var files = di.EnumerateFiles();
                var newestFile = files.OrderBy(x=> x.CreationTime).First();
                var successfullyParsedFileNameToGuid = Guid.TryParse(newestFile.Name,out id);
                if(!successfullyParsedFileNameToGuid){
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Was not able to get GUID from file {newestFile.Name}");
                    Console.ResetColor();
                    return null;
                }
            }
            using (StreamReader reader = new StreamReader($"{path}\\{id}"))
            {
                var text = await reader.ReadToEndAsync();
                var ret = JsonConvert.DeserializeObject<AssignmentWrapper>(text);
                return ret;
            }
        }
        private static async Task SaveAssignmentToDiskAsync(Guid id,string v)
        {
            string path = @"D:\Dev\HackerCampClient\Data\Assignments\"+id;
            Console.WriteLine($"Writing assignment to disk-> {path}");
            await File.WriteAllTextAsync(path,v);
        }
    }
#endregion
}
