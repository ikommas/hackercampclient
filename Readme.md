# 1) Clone the repo
# 2) dotnet restore
# 3) open Program.cs and change the name -> your name
# 4) run the program once to see the data (it will persist the data to file so you can run it multiple times without the data changing)
# 4) open an IDE and implement the CalculateFamilyTree() method 
# 5) When you have the answer correct you will get a HTTP 200 OK from the PostAnswer() method and you are done
# 6) Go to http://hackercampapi.azurewebsites.net/api/FamilyAssignment/leaderboard to see the overall results
